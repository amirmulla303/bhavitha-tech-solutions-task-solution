package com.example.myapplication22;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class Menu_Adapter extends RecyclerView.Adapter<Menu_Adapter.ViewHolder> {
    private ItemClickListener clickListener;
    private Context context;
    private List<Menu_Type> list1;

    public Menu_Adapter(Context context, List<Menu_Type> list) {
        this.context = context;
        this.list1 = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.cat_menu_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Menu_Type menu = list1.get(position);
      //  holder.subfoodid=menu.getFoodid();
      //  holder.des=menu.getDescrip();
        holder.menuprice1.setText("Price: " +menu.getDescrip());
        holder.menuname.setText("Created At: " +menu.getMenuname());
        holder.menuprice.setText("Name: " +menu.getMenuprice());
              holder.menucreate.setText("ID: " +menu.getFoodid());
        Glide.with(context).load(menu.getMenuimage())
                .placeholder(R.drawable.ch1)
                .error(R.drawable.ch1)
                .into(holder.ivFish1);
        holder.fimage=menu.getMenuimage();
    }

    @Override
    public int getItemCount() {
        return list1.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView menuname,menuprice,menucreate,menuprice1;
        public ImageView ivFish1;
        Button addcart;
        String subfoodid,des,fimage;

        public ViewHolder(View itemView) {
            super(itemView);
            menuname = itemView.findViewById(R.id.menuname);
            menuprice = itemView.findViewById(R.id.menuprice);
            menucreate = itemView.findViewById(R.id.menucreate);
            menuprice1 = itemView.findViewById(R.id.menuprice1);

            ivFish1=itemView.findViewById(R.id.menuimage);
         //   addcart=itemView.findViewById(R.id.addcart);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
