package com.example.myapplication22;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String url="https://5ffb4cd563ea2f0017bdb052.mockapi.io/bhavitha";
    private List<Menu_Type> catList;
    private RecyclerView.Adapter adapter1;
    RecyclerView cat_view;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;
    LinearLayoutManager HorizontalLayout;
    Button sort;

    private boolean ascending = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sort=(Button)findViewById(R.id.placeorder);
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
            }
        });

        cat_view = findViewById(R.id.view_catlist);

        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        // Set LayoutManager on Recycler View
        cat_view.setLayoutManager(
                RecyclerViewLayoutManager);
        HorizontalLayout = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        cat_view.setLayoutManager(HorizontalLayout);

       // url= BaseUtils.GET_SUB_RELATED_PRODUCT+foodid;
        catList = new ArrayList<>();


        adapter1 = new Menu_Adapter(getApplicationContext(),catList);
        cat_view.setAdapter(adapter1);





        getData2();
    }

    private void getData2() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject1 = response.getJSONObject(i);

                        Menu_Type cat = new Menu_Type();
                        cat.setFoodid(jsonObject1.getString("id"));
                        cat.setMenuname(jsonObject1.getString("createdAt"));
                        cat.setMenuprice(jsonObject1.getString("name"));
                        cat.setMenuimage(jsonObject1.getString("avatar"));
                        cat.setDescrip(jsonObject1.getInt("price"));

                        //  cat.setMenuimage(jsonObject1.getString("Image1"));

                        catList.add(cat);


                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
                adapter1.notifyDataSetChanged();
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }


    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject1 = response.getJSONObject(i);

                        Menu_Type cat = new Menu_Type();
                        cat.setFoodid(jsonObject1.getString("id"));
                        cat.setMenuname(jsonObject1.getString("createdAt"));
                        cat.setMenuprice(jsonObject1.getString("name"));
                        cat.setMenuimage(jsonObject1.getString("avatar"));
                        cat.setDescrip(jsonObject1.getInt("price"));

                        //  cat.setMenuimage(jsonObject1.getString("Image1"));

                        catList.add(cat);

                        Collections.sort(catList, new Comparator<Menu_Type>() {
                            @Override
                            public int compare(Menu_Type lhs, Menu_Type rhs) {
                                return lhs.getDescrip().compareTo(rhs.getDescrip());

                            }

                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
                adapter1.notifyDataSetChanged();
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

