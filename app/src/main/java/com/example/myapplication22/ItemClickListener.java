package com.example.myapplication22;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);

}
