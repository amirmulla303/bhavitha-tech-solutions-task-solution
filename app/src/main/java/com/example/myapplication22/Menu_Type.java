package com.example.myapplication22;

public class Menu_Type {
    String foodid;
    String menuname;
    String menuprice;
    String menuimage;
    Integer descrip;

    public Menu_Type() {
    }

    public Menu_Type(String foodid, String menuname, String menuprice, String menuimage, Integer descrip) {
        this.foodid = foodid;
        this.menuname = menuname;
        this.menuprice = menuprice;
        this.menuimage = menuimage;
        this.descrip = descrip;
    }

    public String getFoodid() {
        return foodid;
    }

    public void setFoodid(String foodid) {
        this.foodid = foodid;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getMenuprice() {
        return menuprice;
    }

    public void setMenuprice(String menuprice) {
        this.menuprice = menuprice;
    }

    public String getMenuimage() {
        return menuimage;
    }

    public void setMenuimage(String menuimage) {
        this.menuimage = menuimage;
    }

    public Integer getDescrip() {
        return descrip;
    }

    public void setDescrip(Integer descrip) {
        this.descrip = descrip;
    }
}
